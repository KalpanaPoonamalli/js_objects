function pairs(inventoryObj){
    let array = [];
    for (let key in inventoryObj){
        array[array.length] = [key, inventoryObj[key]]
    }
    return array;
}

module.exports = pairs;
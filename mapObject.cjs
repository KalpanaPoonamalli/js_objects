function mapObject(inventoryObj, cb){
    let array = []
    for (let key in inventoryObj){
        let callBackFunction = cb(key, inventoryObj[key])
        array[array.length] = callBackFunction
    }
    return array;
}

module.exports = mapObject;
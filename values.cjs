function values(inventoryObj){
    let array = []
    for (let key in inventoryObj){
        array[array.length] = inventoryObj[key];
    }
    return array;
}

module.exports = values;
function invert(inventoryObj){
    let array = {};
    for (let key in inventoryObj){
        array[JSON.stringify(inventoryObj[key])] = key;
    }
    return array;
}

module.exports = invert;

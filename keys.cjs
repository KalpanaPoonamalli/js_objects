function keys(inventory){
    let array = []
    for (let key in inventory){
         array[array.length] = key  // or using push  -->  array.push(key) 
    }
    return array;
 }
 
 module.exports = keys;